# Divisiones explicadas

Este es proyecto es un software que explica divisiones de forma automática. La
idea desarrollar paso a paso una división, de manera que se pueda usar de
referencia en el futuro.

## Capacidades
Este es un checklist de las capacidades que es estarán implementando

- [x] Divisiones de un dígito en el divisor
- [x] Divisiones de más de un dígito en el divisor
- [ ] Divisiones donde el consiente tiene coma (con precisión ajustable)
- [ ] Divisiones donde cualquier operando tiene coma (con precisión ajustable)

## Instrucciones

Para instalar primero se necesita tener [Python3](https://www.python.org/) y
[Poetry](https://python-poetry.org/docs/basic-usage/). Si su sistema es Linux,
entonces ya tiene Python3 instalado, solo debe seguir los siguientes pasos para
tener el sistema funcionando:

```
sudo apt install python3-virtualenv
curl -sSL https://install.python-poetry.org | python3 -
```

Luego es necesario clonar el repositorio para tener una copia local y poder
ejecutarlo. En este caso se está clonando el repositorio en el directorio
`~/local/src` pero se puede hacer en cualquier otro lugar que prefiera.

```
cd ~/local/src
git clone https://gitlab.com/degv364/divisiones_exp.git
cd divisiones_exp
```

Una vez está dentro del repositorio, ya se puede utilizar Poetry para
ejecutarlo.

```
poetry run divisiones -i 23 -d 2
```

Ese comando va a crear las explicaciones de dividir `23` entre `2` y lo va a
guardar en un archivo llamado `ejemplo.md`. Los parámetros de la aplicación son
los siguientes:

```
  -h, --help            show this help message and exit
  -i DIVIDENDO, --dividendo DIVIDENDO
                        Dividendo, número a dividir
  -d DIVISOR, --divisor DIVISOR
                        Divisor, número por el que se divide
  -n NUM, --number NUM  Si se indica este valor, se generaran N divisiones de
                        ejemplo. Las divisiones serán de números aleatorios
                        generados en un rango desde 1 hasta el valor
                        especificado de divisor y dividendo respectivamente
  -s SAVEAS, --save-as SAVEAS
                        Nombre del archivo donde se guardan los resultados
```

Este programa genera los resultados en markdown. Si se prefiere otro formato se
puede utilizar `pandoc` para convertirlo.

```
pandoc file.md -o file.pdf --pdf-engine=xelatex
```
