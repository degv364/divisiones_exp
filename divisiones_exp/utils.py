# -*- coding: utf-8 -*-
#
# Copyright (c) Daniel García-Vaglio
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


def underline(word):
    return "\u0332"+"\u0332".join(str(word))+"\u0332"


def fill(number, position, total):
    """
    Fill a string such that it fits in the division position
    """
    result = str(number)
    # Add spaces at the end
    end_spaces = "".join([" " for idx in range(total-position-1)])
    result = result + end_spaces
    # Calculate the new length
    length = len(result)
    # Add spaces in the front to align number
    front_spaces = "".join([" " for idx in range(total-length+1)])
    result = front_spaces + result
    return result
