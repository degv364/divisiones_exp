# -*- coding: utf-8 -*-
#
# Copyright (c) Daniel García-Vaglio
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import argparse
from random import randint
from divisiones_exp.division import Division


def main():
    parser = argparse.ArgumentParser(description="Divisiones explicadas")
    parser.add_argument(
        "-i", "--dividendo",
        dest="dividendo",
        type=int,
        required=True,
        help="Dividendo, número a dividir")
    parser.add_argument(
        "-d", "--divisor",
        dest="divisor",
        type=int,
        required=True,
        help="Divisor, número por el que se divide")
    parser.add_argument(
        "-n", "--number",
        dest="num",
        type=int,
        required=False,
        default=1,
        help="Si se indica este valor, se generaran N divisiones de ejemplo."
        " Las divisiones serán de números aleatorios generados en un rango"
        " desde 1 hasta el valor especificado de divisor y dividendo"
        " respectivamente"
    )
    parser.add_argument(
        "-s", "--save-as",
        dest="saveas",
        type=str,
        required=False,
        default="ejemplo.md",
        help="Nombre del archivo donde se guardan los resultados"
    )
    args = parser.parse_args()
    if args.num == 1:
        division = Division(divisor=args.divisor, dividendo=args.dividendo)
        division.generate_steps()
        with open(args.saveas, "w") as archivo:
            archivo.write(division.generate_str())
    else:
        resulting_text = ""
        divisiones = [Division(divisor=randint(1, args.divisor),
                               dividendo=randint(1, args.dividendo))
                      for rn in range(args.num)]
        for example in divisiones:
            example.generate_steps()
            resulting_text += example.generate_str()
        with open(args.saveas, "w") as archivo:
            archivo.write(resulting_text)
