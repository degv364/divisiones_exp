# -*- coding: utf-8 -*-
#
# Copyright (c) Daniel García-Vaglio
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from divisiones_exp.utils import underline, fill


class Division():
    def __init__(self, dividendo, divisor):
        # Dividendo, number to divide
        self.dividendo = dividendo
        # Divsor, number that divides
        self.divisor = divisor
        # Cosiente
        self.cosiente = 0
        # List of dividendo portions that have been brought down
        self.bajados = [0]
        # bajados hisoricos, para poder reconstruir los pasos intermadios
        self.bajados_historicos = []
        self.cosientes_historicos = []
        # List of multiplication results
        self.multiplos = []
        # Tabla
        self.tabla = [idx*self.divisor for idx in range(0, 10)]
        # Explanation
        self.explanation = []

        self.pasos = {
            "bajar": "Se baja el siguiente dígito del dividendo, y revisar si el numero resultante luego de bajar es mayor que el divisor.",
            "buscar": "Buscar en la tabla del divisor el mayor múltiplo que no supere al número bajado.",
            "restar": "Se resta al número bajado menos el múltiplo del divisor.",
            "terminar": "Ya no se puede bajar más dígitos del dividendo, entonces se ha terminado la división.",
            "poner0": " Como se bajó un dígito y no era mayor, se agrega 0 al cosiente.",
            "final": " Se ha calculado el residuo y no se pueden bajar más números del dividendo entonces finaliza la división."
        }

        self.dividendo_length = len(str(self.dividendo))
        self.current_position = 0
        # to keep track of the step  depth we are on
        self.step_depth = 0
        # tokeep track of the step we are on
        self.step_idx = 0

        # String que guarda cómo se ve la division
        self.lines = [""]

    def generate_steps(self):
        """
        This function should generate the steps for the division
        and calculate a correct result
        """

        # El primer paso es bajar
        next_step = self._bajar
        while next_step is not None:
            # Cada paso calcula el paso que le sigue
            self.explanation.append("\n\n## Paso " + str(self.step_idx+1) + "\n\n")
            next_step = next_step()
            self.step_idx += 1
            self.explanation.append("\n"*2)
            self.explanation.append(self._draw_current_step())
            self.explanation.append("\n"*2)

        self.residuo = self.bajados[-1]
        self.verify()

    def verify(self):
        self.correct_cosiente = self.dividendo//self.divisor
        self.correct_residuo = self.dividendo - self.divisor*self.cosiente
        assert self.correct_cosiente == self.cosiente
        assert self.correct_residuo == self.residuo

    def _bajar(self):
        """
        Bajar el siguiente numero del dividendo
        """
        if self.current_position == self.dividendo_length:
            self.explanation.append(self.pasos["terminar"])
            return None

        self.explanation.append(self.pasos["bajar"])
        numero_a_bajar = str(self.dividendo)[self.current_position]
        bajado = str(self.bajados[-1])
        self.bajados[-1] = int(bajado+numero_a_bajar)
        self.bajados_historicos.append(self.bajados[-1])

        if self.bajados[-1] < self.divisor:
            self.cosiente *= 10
            if self.cosiente != 0:
                self.explanation.append(self.pasos["poner0"])
            next_step = self._bajar
        else:
            next_step = self._buscar
            self.step_depth += 1

        self.lines[-1] = fill(self.bajados[-1], self.current_position, self.dividendo_length)

        self.current_position += 1

        return next_step

    def _buscar(self):
        """
        BUscar en la tabla del divisor el numero que sirve
        """
        multiplo = 0
        bajado = self.bajados[-1]
        multiplos_menores = [mul for mul in self.tabla if mul <= bajado]
        multiplo = max(multiplos_menores)
        self.multiplos.append(multiplo)

        self.cosiente = int(str(self.cosiente)+str(multiplo//self.divisor))
        self.cosientes_historicos.append(self.cosiente)
        self.step_depth += 1
        self.lines.append(underline(fill(-multiplo, self.current_position-1, self.dividendo_length)))

        self.explanation.append(self.pasos["buscar"])
        return self._restar

    def _restar(self):
        """
        Restar el multiplo al numero bajado
        """
        resta = self.bajados[-1] - self.multiplos[-1]
        self.bajados.append(resta)
        self.bajados_historicos.append(self.bajados[-1])
        self.step_depth += 1
        self.lines.append(fill(self.bajados[-1], self.current_position-1, self.dividendo_length))
        self.explanation.append(self.pasos["restar"])
        if self.current_position == self.dividendo_length:
            self.explanation.append(self.pasos["final"])
            return None
        return self._bajar

    def _draw_current_step(self):
        """
        Crear un string que representa la division en el paso actual
        """
        division_str = " " + str(self.dividendo) + "|" + underline(self.divisor) + "\n"
        division_str += self.lines[0] + "|"
        if self.cosiente != 0:
            division_str += str(self.cosiente)
        division_str += "\n"

        division_str += "|\n".join(self.lines[1::])
        return "```\n" + division_str + "\n```"

    def generate_str(self):
        hablada = "# Division {} ÷ {} \n\n".format(self.dividendo, self.divisor)
        final = ("\n\n## Resultado\n- Dividendo = {}\n- Divisor = {}" +
                 "\n- Cosiente = {}\n- Residuo = {}\n\n\n").format(
            self.dividendo, self.divisor, self.cosiente, self.residuo
        )

        explanation = hablada + "".join(self.explanation) + final
        return explanation
