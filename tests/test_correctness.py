# -*- coding: utf-8 -*-
#
# Copyright (c) Daniel García-Vaglio
# Author: Daniel Garcia-Vaglio <degv364@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

from random import randint
from divisiones_exp.division import Division


def test_correctness():
    # Generate a bunch of random numbers
    dividendos = [randint(0, 2**20) for idx in range(100000)]

    for dividendo in dividendos:
        divi = Division(divisor=randint(1, 2**20), dividendo=dividendo)
        divi.generate_steps()
